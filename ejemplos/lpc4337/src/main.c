/* Copyright 2020, E. Sergio Burgos.
 * All rights reserved.
 *
 * This file is part bShell library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#include <stdint.h>

#include "../inc/cmdBShell.h"
#include "sysUtils.h"
#include "chip.h"

static cmdInstance cmdSer1;

void sendSerial1(char byte)
{
	Chip_UART_SendBlocking(LPC_USART2,&byte, 1);
}

char reciveSerial1(void)
{
	char byte;
    NVIC_DisableIRQ(USART2_IRQn);
	Chip_UART_ReadBlocking(LPC_USART2, &byte, 1);
	Chip_UART_SendBlocking(LPC_USART2, &byte, 1);
    NVIC_EnableIRQ(USART2_IRQn);
	return byte;
}

void UART2_IRQHandler(void)
{
	uint8_t data = Chip_UART_ReadByte(LPC_USART2);
	cmdProcessNewChar(&cmdSer1, data);
}

int main(void)
{

  uint8_t ch;
  sysInit();

  cmdInit(&cmdSer1, sendSerial1, reciveSerial1);

  while(1);
  return 0;
}
