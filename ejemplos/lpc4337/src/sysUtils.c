#include "chip.h"
#include <lpc_types.h>
#include "sysUtils.h"

const uint32_t ExtRateIn = 0;
const uint32_t OscRateIn = 12000000;

const digitalIO
  leds[] =
  {
     {0x02, 0x02, 0x05, 0x02, SCU_MODE_INACT | SCU_MODE_FUNC4},
     {0x02, 0x0A, 0x00, 0x0E, SCU_MODE_INACT | SCU_MODE_FUNC0},
	 {0x02, 0x0B, 0x01, 0x0B, SCU_MODE_INACT | SCU_MODE_FUNC0},
	 {0x02, 0x0C, 0x01, 0x0C, SCU_MODE_INACT | SCU_MODE_FUNC0},
	 {0x02, 0x00, 0x05, 0x00, SCU_MODE_INACT | SCU_MODE_FUNC4},
	 {0x02, 0x01, 0x05, 0x01, SCU_MODE_INACT | SCU_MODE_FUNC4}

  };

const digitalIO
  keys[] =
  {
    {0x01, 0x00, 0x00, 0x04, SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN},
    {0x01, 0x01, 0x00, 0x08, SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN},
	{0x01, 0x02, 0x00, 0x09, SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN},
	{0x01, 0x06, 0x01, 0x09, SCU_MODE_INACT | SCU_MODE_FUNC0 | SCU_MODE_INBUFF_EN}
  };



void sysInit(void)
{
  int i;
  Chip_SetupXtalClocking();
  SystemCoreClockUpdate();
  StopWatch_Init();
  fpuInit();
  Chip_GPIO_Init(LPC_GPIO_PORT);

  for(i = 0; i < sizeof(leds)/sizeof(digitalIO); i++)
  {
	  Chip_SCU_PinMuxSet(leds[i].hwPort, leds[i].hwPin, leds[i].modo);
	  Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, leds[i].gpioPort, leds[i].gpioPin);
	  Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT, leds[i].gpioPort, leds[i].gpioPin);
  }

  for(i = 0; i < sizeof(keys)/sizeof(digitalIO); i++)
  {
	  Chip_SCU_PinMuxSet(keys[i].hwPort, keys[i].hwPin, keys[i].modo);
	  Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, keys[i].gpioPort, keys[i].gpioPin);
  }


  Chip_SCU_PinMuxSet(7, 1, SCU_MODE_PULLDOWN | SCU_MODE_FUNC6);
  Chip_SCU_PinMuxSet(7, 2, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC6);
  Chip_UART_Init(LPC_USART2);
  Chip_UART_ConfigData(LPC_USART2, UART_LCR_WLEN8 | UART_LCR_SBS_1BIT | UART_LCR_PARITY_DIS);
  Chip_UART_SetBaud(LPC_USART2, 115200);
  Chip_UART_SetupFIFOS(LPC_USART2, (UART_FCR_FIFO_EN | UART_FCR_RX_RS |
  							UART_FCR_TX_RS | UART_FCR_TRG_LEV3));
  Chip_UART_IntEnable(LPC_USART2, UART_IER_RBRINT);
  NVIC_EnableIRQ(USART2_IRQn);
  Chip_UART_TXEnable(LPC_USART2);

}


void print(const char *txt)
{
	Chip_UART_SendBlocking(LPC_USART2, (const void*) txt, strlen(txt));
}

void hwReset (void)
{
	 Chip_RGU_TriggerReset(RGU_CORE_RST);
}

