#ifndef INC_SYSUTILS_H_
#define INC_SYSUTILS_H_

#include <stdint.h>

typedef struct
{
	uint8_t hwPort;
	uint8_t hwPin;
	uint8_t gpioPort;
	uint8_t gpioPin;
	uint16_t modo;
}digitalIO;

void sysInit(void);
void print(const char *txt);

void hwReset (void);

#endif /* INC_SYSUTILS_H_ */
