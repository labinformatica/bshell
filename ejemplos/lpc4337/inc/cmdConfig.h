/* Copyright 2020, E. Sergio Burgos.
 * All rights reserved.
 *
 * This file is part bShell library for microcontrollers.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef CMD_INC_CMDCONFIG_H_
#define CMD_INC_CMDCONFIG_H_

#include "cmdColors.h"

/*
 * MAXARGCNT: cantidad máxima de argumentos para un comando particular
 *
 * */
#define MAXARGCNT 8

/*
 * LINELEN: máxima longitud de una línea de comandos
 *
 * */
#define LINELEN 101

/*
 * USEHISTORY: si está definida, activa el uso de historial de comandos
 *
 * */
#define USEHISTORY

/*
 * HISTORYLEN: cantidad de niveles históricos que se desea utilizar.
 *
 * */
#define HISTORYLEN 10

/*
 * BKGTERM: color de fondo de la terminal
 * */

#define BKGTERM bBlack

/*
 * FRGTERM: color de primer plano de la terminal
 * */

#define FRGTERM fLightGreen

/*
 * PROMPT: mensaje utilizado para indicar que el microcontrolador está listo para recibir
 *  comandos.
 * */

#define PROMPT "bShell:> "
#define PROMPTLEN 9

/*
 * CMDNF: mensaje utilizado para indicar que el comando no se encuentra disponible
 *
 * */
#define CMDNF "Command not found:"

/*
 * CMDSHOWLOGO: muestra un logo usando ascii art al inicio de la sesión.
 *
 * */
#define CMDSHOWLOGO


#endif /* CMD_INC_CMDCONFIG_H_ */
