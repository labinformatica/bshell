# bShell en el LPC4337

En esta carpeta encontrarás un proyecto básico que demuestra como configurar y utilizar bShell en la [EDU-CIAA-NXP](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:edu-ciaa:edu-ciaa-nxp) que utiliza como microcontrolador el LPC4337.

## Compilando el proyecto.
Es un proyecto basado en un makefile personalizado, por lo que, si desea utilizar Eclipse, deberá importarlo como un proyecto de lenguaje C basado en Makefiles. 
La configuración del proyecto debe actualizarce según las rutas locales y herramientas utilizadas. 
Para esto modifique el archivo **config.mk**, fundamentalmente 3 parámetros:
* GCCPATH (línea 3): especifica la ruta al compilador utilizado. Si trabajas en Linux  e instalas el compilador gcc para arm desde el repositorio oficial, seguramente debería ser /usr. Si instalas una versión particular de este compilador depende del directorio donde lo hayas descomprimido.
* OPENOCD_BIN (línea 17): ruta donde se encuentra el binario de OpenOCD. Esto es utilizado ya que es posible compilar y programar la EDU-CIAA-NXP directamente desde este makefile, sin necesidad de utilizar el IDE de Eclipse. 
* BUILDTYPE (línea 20): establece si se desean incluir o no símbolos de depuración en el binario. Los valores posibles para este parámetro son **debug** o **release**.

Si prefieres construir la aplicación desde una terminal linux, con las opciones anteriores modificadas adecuadamente puedes hacerlo del siguiente modo:

```
make clean
make 
make program
```

## Detalles de interés

Este es un simple ejemplo tendiente a mostrar cómo puede utilizarce bShell. La librería requiere 2 funciones que permitan enviar y recibir 1 byte sobre el canal de comunicación sobre el que se desee interactuar. 
Estas funciones son utilizadas por la libería a través de variables definidas de tipo  `cmdInstance`, esta estructura permite mantener la información asociada a un terminal de comandos. Si se deseara utilizar múltiples interfaces de comandos en la misma aplicación solo deben crearse más variables de este tipo. La definición de esta estructura puede observarse en `src/main.c` (línea 39), luego las definiciones de las estructuras para enviar y recibir un byte (líneas 41 y 46).

La interacción con la librería se da a través de 2 funciones y las definiciones de los comandos personados que se utilicen. Las funciones son:

`void cmdInit(cmdInstance * inst, sendFcnPtr sendFcn, reciveFcnPtr rcvFcn)`

Los argumentos de esta función corresponden con:
* La dirección de memoria de una variable de tipo `cmdInstance`
* La dirección de memoria de una función que permita enviar un byte 
* La dirección de memoria de una función que permita recibir un byte.  

Esta función debe invocarse durante el proceso de inicialización de la terminal (línea 68). 

`void cmdProcessNewChar(cmdInstance * inst, uint8_t newByte)`

Esta función debe invocarse cada vez que se ha recibido un nuevo valor desde la interfaz de comunicaciones utilizada. En una aplicación típica, la invocación se realizará desde una interrupción que se dispara cada vez que se recibe un caracter. 
El argumento es la dirección de memoria de la variable de tipo `cmdInstance` y el valor recibido por la interfaz.
Cada vez que se detecte que se ha dado entrada a un nuevo comando, esta misma función se encargará de identificar el comando a ejecutar y realizará la invocación correspondiente. 

### Definición de comandos

Los comandos pueden definirse en cualquier archivo de código fuente, y para esto solo es necesario agregar definiciones como las realizadas en los archivos `src/commands_part1.c` y `src/commands_part2.c`. 
Específicamente un comando es una función que tiene un nombre asociado y una descripción. La función debe tener el siguiente prototipo:

```
uint8_t nombreFuncion(cmdInstance *inst)
```

El valor retornado no se utiliza en la versión actual de la librería pero, en el futuro, será utilizado a fin de tener conocimiento sobre el éxito o no de la operación realizada. 
El argumento será dado automáticamente y corresponde con la variable `cmdInstance` vinculada a la terminal. A partir de esta será posible:

* Acceder a los argumentos utilizados: a través de los campos argv y argc. Del mismo modo que como es usual en aplicaciones desarrolladas en lenguaje C, estos valores se corresponden con el valor de cada uno de los arguentos (argv es un vector de cadenas de caracteres) y argc es la cantidad de argumentos utilizado (incluyendo el nombre del comando, por lo que tomará como mínimo valor 1).
* Interactuar con el usuario de ser necesario a través de las funciones de entrada salida, que son accesibles a través de los campos `sndPort` y `rcvPort`. Estos campos tendrán las direcciones de memoria de las funciones indicadas durante la inicialización para enviar y recibir bytes sobre el canal utilizado por la terminal.


Una vez definida cada función que se desea funcione como un comando, es necesario agregarla en una definición como la siguiente:

```
__attribute__ ((used,section(".cmdCommands")))
const commandEntry commandDef [] = 
{
  {
    "copy", /* Command name */
    nombreFuncion, /* Associated function */
    "copy: copia dos área de memoria" /* Command help */
  },
  {
    "rm", /* Command name */
    otraFuncion, /* Associated function */
    "rm: borra una dirección de memoria" /* Command help */
  }
};  
```

La estructura `commandEntry` tiene 3 campos, el nombre del comando que será utilizado para identificar cuando se lo invoca, el nombre de la función asociada (que debe estar previamente definida) y una cadena de documentación. El nombre de la estructura (`commandDef` en el ejemplo), no es significativo pero no debe repetirse. En el ejemplo se definen dos comandos, pero podrían definirse 1 o más. 

Es muy importante que la definición anterior se realice junto a los atributos *used* y *section*, esto aloja el bloque en una posición especial de la memoria de programa donde será identificada por la librería. 
Para lograr este efecto, el script de enlazado deberá incluir una definición (en general en la sección text) como la siguiente:

```        
  /* Comandos insertados para bShell */
    commandsBegin = ABSOLUTE(.);
    KEEP(*(.cmdCommands))
    commandsEnd = ABSOLUTE(.);
  /* Fin comandos insertados para bShell */
 ```
 
 En este ejemplo encontrá esta definición en el archivo `ld/areas.ld` línea 45.